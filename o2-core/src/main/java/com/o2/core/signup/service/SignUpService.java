package com.o2.core.signup.service;

import org.springframework.stereotype.Component;

import com.o2.core.common.model.UserContext;
import com.o2.core.signup.model.SignUpResultVO;
import com.o2.core.signup.model.SignUpVO;

@Component
public class SignUpService {

	public SignUpResultVO process(UserContext userCtx, SignUpVO signUpVO) {
		System.out.println(signUpVO);
		return createDummyResponse();
	}
	
	private SignUpResultVO createDummyResponse() {
		SignUpResultVO respVO=new SignUpResultVO();
		respVO.setGeneratedUserId("UserID_123");
		return respVO;
	}

}
