package com.o2.core.signup.model;

import com.o2.core.common.model.GenericRequestVO;

public class SignUpRequestVO extends GenericRequestVO {
	private String emailId;
	private String password;
	private String firstName;
	
	@Override
	public String toString() {
		return "SignUpRequestVO ["+super.toString() + ",emailId=" + emailId + ", password=" + password
				+ ", firstName=" + firstName + "]";
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	
}
