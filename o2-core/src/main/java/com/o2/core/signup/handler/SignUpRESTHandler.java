package com.o2.core.signup.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.o2.core.common.model.GenericRequestVO;
import com.o2.core.common.model.GenericResponseVO;
import com.o2.core.common.model.ResponseHeader;
import com.o2.core.common.model.Status;
import com.o2.core.common.model.UserContext;
import com.o2.core.signup.model.SignUpResultVO;
import com.o2.core.signup.model.SignUpVO;
import com.o2.core.signup.service.SignUpService;

@Component
public class SignUpRESTHandler {

	@Autowired
	private SignUpService signupService;

	public GenericResponseVO<SignUpResultVO> handle(
			UserContext userCtx, GenericRequestVO<SignUpVO> signupRequestVO) {
		GenericResponseVO<SignUpResultVO> responseVO = new GenericResponseVO<SignUpResultVO>();
		responseVO.setBody(signupService.process(userCtx,signupRequestVO.getBody()));
		
		ResponseHeader header = new ResponseHeader();
		header.setStatus(new Status("SUCCESS", "000", "Signup is successful."));
		
		responseVO.setHeader(header);
		return responseVO;
	}

}
