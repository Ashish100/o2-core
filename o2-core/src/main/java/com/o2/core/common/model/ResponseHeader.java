package com.o2.core.common.model;

public class ResponseHeader {
	
	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ResponseHeader [status=" + status + "]";
	}
	

}
