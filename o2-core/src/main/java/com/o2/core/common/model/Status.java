package com.o2.core.common.model;

public class Status {
	public static final String SUCCESS="SUCCESS";
	public static final String FAILURE="FAILURE";
	
	private String statusCode;
	private String statusName;
	private String statusDescription;
	
	public Status(String statusName, String statusCode, String statusDescription) {
		this.statusCode=statusCode;
		this.statusName=statusName;
		this.statusDescription=statusDescription;
	}

	@Override
	public String toString() {
		return "Status [statusCode=" + statusCode + ", statusName="
				+ statusName + ", statusDescription=" + statusDescription + "]";
	}
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

}
