package com.o2.core.common.model;

public class RequestHeader {
	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "RequestHeader [sessionId=" + sessionId + "]";
	}

	
}
