package com.o2.core.common.model;

public class GenericRequestVO<T> {
	
	private RequestHeader header;
	
	private T body;

	public RequestHeader getHeader() {
		return header;
	}

	public void setHeader(RequestHeader header) {
		this.header = header;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "GenericRequestVO [header=" + header + ", body=" + body + "]";
	}
	
	
}
