package com.o2.core.common.model;

public class GenericResponseVO<T> {
	
	private ResponseHeader header;
	
	private T body;

	public ResponseHeader getHeader() {
		return header;
	}

	public void setHeader(ResponseHeader header) {
		this.header = header;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "GenericResponseVO [header=" + header + ", body=" + body + "]";
	}


	

}
