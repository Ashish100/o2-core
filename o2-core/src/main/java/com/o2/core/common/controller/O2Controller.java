package com.o2.core.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.o2.core.common.model.GenericRequestVO;
import com.o2.core.common.model.GenericResponseVO;
import com.o2.core.common.model.UserContext;
import com.o2.core.signup.handler.SignUpRESTHandler;
import com.o2.core.signup.model.SignUpResultVO;
import com.o2.core.signup.model.SignUpVO;

@Controller
public class O2Controller {

	@Autowired
	private SignUpRESTHandler signUpRequestHandler;

	/**
	 * This method accepts incoming sign up requests.
	 * 
	 * @param signupRequestVO
	 * @return
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
	public @ResponseBody
	GenericResponseVO<SignUpResultVO> signUp(
			@RequestBody(required = false) GenericRequestVO<SignUpVO> signupRequestVO) {
		UserContext userCtx=getUserContext();
		return signUpRequestHandler.handle(userCtx,signupRequestVO);
	}

	private UserContext getUserContext() {
		return null;
	}

	/**
	 * This is a test method for testing connection.
	 * 
	 * @param inputField
	 * @return
	 */
	@RequestMapping(value = "/test", method = RequestMethod.GET, consumes = "application/json", produces = "application/json;charset=UTF-8")
	public @ResponseBody
	String test(@RequestParam("key1") String inputField) {
		return "ECHO " + inputField;
	}

}
